<?php
    //Starting with curl init
    $curl = curl_init();
    //The url you want to scrape
    $url = "https://nos.nl/weer/";

    // set the url you want to scrape
    curl_setopt($curl, CURLOPT_URL, $url);
    // This will let you scrape HTTPS websites
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    // This will let you store the results in an variable instead of showing it in the browser
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    //execute the curl
    $result = curl_exec($curl);

    // "Create" the document
    $dom = new DOMDocument();

    // Clear the errors
    libxml_use_internal_errors(true);
    libxml_clear_errors();

    $dom->loadHTML($result);

    //Searching with XPath
    $xpath = new DomXPath($dom);

    $datas = $xpath->query('//*[@class="show-from--tablet"]');
    $maxt = $xpath->query('//*[@class="weather-table__temp"]');
    $mint = $xpath->query('//*[@class="weather-table__temp text-grey"]');

    // Close the curl
    curl_close($curl);
