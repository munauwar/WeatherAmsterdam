<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Weather Amsterdam</title>
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
        <?php include('php/weather.php'); ?>
    </head>
    <body>
        <table class="table">
            <tr>
                <th>Dag</th>
                <?php
                    foreach($datas as $data) {
                        echo '<td>' . $data->nodeValue . '</td>';
                    }
                ?>
            </tr>
                <th>Max</th>
                <?php
                     foreach($maxt as $maxtemp) {
                        echo '<td>'. $maxtemp->nodeValue . '</td>';
                    }
                ?>
            <tr>
                <th>Min</th>
                <?php
                    foreach($mint as $mintemp) {
                        echo '<td>' . $mintemp->nodeValue . '</td>';
                    }
                ?>
            </tr>
        </table>
    </body>
</html>
